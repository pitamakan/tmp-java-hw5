package com.company.currency.service;

import com.company.currency.dto.CurrencyDTO;
import com.company.currency.dto.CurrencyRateDTO;
import com.company.currency.dto.ExchangeRatesResponse;
import com.company.currency.client.ExchangeRatesApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.company.currency.util.CurrencyUtils.calcRate;

@Service
public class CurrencyRateService implements ICurrencyRateService {

  private final Map<String, CurrencyDTO> statistics =
          CURRENCY_LIST.stream().collect(Collectors.toMap(x -> x, x-> new CurrencyDTO()));

  @Autowired
  private ExchangeRatesApi rateServiceProxy;

  @Override
  public CurrencyRateDTO getRate(String base, String target, BigDecimal amount, LocalDate date) {
    if (!CURRENCY_LIST.contains(base)) {
      throw new RuntimeException("Unknown currency: " + base);
    }
    if (!CURRENCY_LIST.contains(target)) {
      throw new RuntimeException("Unknown currency: " + target);
    }
    if (Objects.equals(base, target)) {
      if (amount != null) {
        return new CurrencyRateDTO(BigDecimal.ONE, amount.multiply(BigDecimal.ONE), base + "/" + target);
      }
      return new CurrencyRateDTO(BigDecimal.ONE, null, base + "/" + target);
    }
    if (Objects.equals(target, "EUR")) {
      ResponseEntity<ExchangeRatesResponse> rate = rateServiceProxy.getCurrencyRate(date, date, base);
      statistics.get(base).incAmountOfRequests();
      BigDecimal resultRate = calcRate(rate.getBody().getDataSets().get(0).getSeries().getFirstSeries().getObservations().getRate().get(0), BigDecimal.ONE);
      if (amount != null) {
        return new CurrencyRateDTO(resultRate, amount.multiply(resultRate), base + "/" + target);
      }
      return new CurrencyRateDTO(resultRate,
              null,
              base + "/" + target);
    }
    CurrencyRateDTO first = getRate(target, "EUR", amount, date);
    CurrencyRateDTO second = getRate(base, "EUR", amount, date);
    BigDecimal resultRate = calcRate(first.getRate(), second.getRate());
    if (amount != null) {
      return new CurrencyRateDTO(resultRate, amount.multiply(resultRate), base + "/" + target);
    }
    return new CurrencyRateDTO(resultRate, null, base + "/" + target);
  }

  @Override
  public Map<String, CurrencyDTO> getStatistics() {
    return statistics.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }
}
