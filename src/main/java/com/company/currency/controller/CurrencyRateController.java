package com.company.currency.controller;

import com.company.currency.dto.CurrencyDTO;
import com.company.currency.dto.CurrencyRateDTO;
import com.company.currency.service.CurrencyRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

@RestController
public class CurrencyRateController implements com.company.currency.controller.CurrencyRateApi {

  @Autowired
  private CurrencyRateService service;

  @Override
  @GetMapping(value = "/api/exchange-rate/{target}/{base}", produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody CurrencyRateDTO getRate(
          @PathVariable String target,
          @PathVariable String base,
          @RequestParam(required = false) BigDecimal amount,
          @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate day
          ) {
    if (day == null) {
      day = LocalDate.now().minusDays(2);
    }
    return service.getRate(target, base, amount, day);
  }

  @Override
  @GetMapping(value = "/api/exchange-rate/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody Map<String, CurrencyDTO> getCurrencyStatistics() {
    return service.getStatistics();
  }
}
