package com.company.currency.testdata;

import com.company.currency.dto.ExchangeRatesResponse;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;

public class TestData {

    public static ResponseEntity<ExchangeRatesResponse> getExchangeRatesApiResponse(LocalDate day, BigDecimal rate) {
        return ResponseEntity.ok(ExchangeRatesResponse.builder()
            .dataSets(
                List.of(new ExchangeRatesResponse.DataSets(
                    "Replace",
                    day.format(DateTimeFormatter.ISO_DATE),
                    new ExchangeRatesResponse.Series(new ExchangeRatesResponse.FirstSeries(new ExchangeRatesResponse.Observations(List.of(rate)))))))
            .build());
    }
}
