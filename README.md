Способы взаимодействия с проектом:

Две основные команды:
1. Курс валют `/api/exchange-rate/{target}/{base}?day=yyyy-mm-dd&amount={amount}`  
`target`, `base` - аббревиатура валют из списка  
`day` - дата, за которую взять курс (необязательный, при отсутствии даты берётся текущая дата - 2 дня, по причине задержки данных)  
`amount` - количество валюты, которую надо сконвертировать

Пример:  
`curl 'http://localhost:8080/api/exchange-rate/HUF/USD?day=2021-04-27&amount=20'`
```
{"rate":0.0034,"pair":"HUF/USD","converted_amount":0.0680}
```


`curl 'http://localhost:8080/api/exchange-rate/EUR/USD'`
```
{"rate":1.0385,"pair":"EUR/USD"}
```


`curl 'http://localhost:8080/api/exchange-rate/EUR/ABD'`
```
{"status":400,"title":"Unknown currency: ABD","description":""}
```
2. Запрос статистики `/api/exchange-rate/statistics`

Пример:  
`curl 'http://localhost:8080/api/exchange-rate/statistics'`
```
{"CHF":{"amount_of_requests":0},"HRK":{"amount_of_requests":0},"MXN":{"amount_of_requests":0},"ZAR":{"amount_of_requests":0},"INR":{"amount_of_requests":0},"CNY":{"amount_of_requests":0},"THB":{"amount_of_requests":0},"AUD":{"amount_of_requests":0},"ILS":{"amount_of_requests":0},"KRW":{"amount_of_requests":0},"JPY":{"amount_of_requests":0},"PLN":{"amount_of_requests":0},"GBP":{"amount_of_requests":0},"IDR":{"amount_of_requests":0},"HUF":{"amount_of_requests":2},"PHP":{"amount_of_requests":0},"TRY":{"amount_of_requests":0},"RUB":{"amount_of_requests":0},"ISK":{"amount_of_requests":0},"HKD":{"amount_of_requests":0},"EUR":{"amount_of_requests":0},"DKK":{"amount_of_requests":0},"USD":{"amount_of_requests":4},"CAD":{"amount_of_requests":0},"MYR":{"amount_of_requests":0},"BGN":{"amount_of_requests":0},"NOK":{"amount_of_requests":0},"RON":{"amount_of_requests":0},"SGD":{"amount_of_requests":0},"CZK":{"amount_of_requests":0},"SEK":{"amount_of_requests":0},"NZD":{"amount_of_requests":0},"BRL":{"amount_of_requests":0}}
```
